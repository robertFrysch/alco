#pragma once

#include <algorithm>
#include <any>
#include <array>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <numeric>
#include <span>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

namespace alco {

// Generic template code used for all polymorphic classes
// ======================================================

template <typename Interface>
class PolymorphicFunctor
{
public:
    template <typename ConcreteType>
        requires (!std::same_as<std::remove_cvref_t<ConcreteType>, PolymorphicFunctor<Interface>>)
    PolymorphicFunctor(ConcreteType&& object)
        : m_obj{std::forward<ConcreteType>(object)}
        , m_getterFun{[](std::any* storage) noexcept -> Interface* { return std::any_cast<std::remove_cvref_t<ConcreteType>>(storage); }}
    {
    }

    Interface* operator->()             noexcept { return get(); }
    const Interface* operator->() const noexcept { return get(); }

    template <typename... Args>
    auto operator()(Args... args)       { return get()->operator()(std::forward<Args>(args)...); }
    template <typename... Args>
    auto operator()(Args... args) const { return get()->operator()(std::forward<Args>(args)...); }


private:
    std::any m_obj;
    Interface* (*m_getterFun)(std::any*) noexcept;

    Interface*       get()       noexcept { return m_getterFun(&m_obj); }
    const Interface* get() const noexcept { return m_getterFun(const_cast<std::any*>(&m_obj)); }
};


// Overload helper
// ======================================================

template <class... Callables> struct Overload : Callables... { using Callables::operator()...; };
template <class... Callables> Overload(Callables...) -> Overload<Callables...>; // required only for backwards compatibility to Clang


// Data types
// ======================================================

template <typename T>
concept ArithmeticType = std::is_arithmetic_v<T>;

using DefaultArithmeticType = double;

template <ArithmeticType T = DefaultArithmeticType>
using Number = T;

template <ArithmeticType T = DefaultArithmeticType>
using Vector = std::vector<T>;

template <ArithmeticType T = DefaultArithmeticType>
using VectorView = std::span<const T>;

template <ArithmeticType T = DefaultArithmeticType>
using Data = std::variant<Number<T>, Vector<T>, VectorView<T>>;

template <ArithmeticType T = DefaultArithmeticType>
class FunctionResult
{
public:
    FunctionResult() = default;
    FunctionResult(Data<T> a) : m_data{std::move(a)}, m_numResults{1} {}
    FunctionResult(Data<T> a, Data<T> b) : m_data{std::move(a), std::move(b)}, m_numResults{2} {}
    FunctionResult(Data<T> a, Data<T> b, Data<T> c) : m_data{std::move(a), std::move(b), std::move(c)}, m_numResults{3} {}
    FunctionResult(Data<T> a, Data<T> b, Data<T> c, Data<T> d) : m_data{std::move(a), std::move(b), std::move(c), std::move(d)}, m_numResults{4} {}

    static FunctionResult fromDataVector(std::vector<Data<T>> v)
    {
        FunctionResult<T> ret;
        for(auto& res : v)
            ret.add(std::move(res));
        return ret;
    }

    void add(Data<T> d)
    {
        if(m_numResults == m_data.size())
            throw std::runtime_error{"A FunctionResult cannot hold more than four Data elements."};

        m_data[m_numResults++] = std::move(d);
    }

    template <typename DataType>
    const DataType& get(std::size_t n = 0) const { assertN(n); return std::get<DataType>(m_data[n]); }
    const Data<T>&  get(std::size_t n = 0) const { assertN(n); return m_data[n]; }
    template <typename DataType>
    DataType&       get(std::size_t n = 0)       { assertN(n); return std::get<DataType>(m_data[n]); }
    Data<T>&        get(std::size_t n = 0)       { assertN(n); return m_data[n]; }

    Number<T> getNumber(std::size_t n = 0) const { return get<Number<T>>(n); }
    Vector<T> getVector(std::size_t n = 0) const &
    {
        return std::holds_alternative<Vector<T>>(get(n))
                   ? get<Vector<T>>(n)
                   : Vector<T>{begin(get<VectorView<T>>(n)), end(get<VectorView<T>>(n))};
    }
    Vector<T> getVector(std::size_t n = 0) &&
    {
        return std::holds_alternative<Vector<T>>(get(n))
                   ? std::move(get<Vector<T>>(n))
                   : Vector<T>{begin(get<VectorView<T>>(n)), end(get<VectorView<T>>(n))};
    }
    VectorView<T> getVectorView(std::size_t n = 0) const
    {
        return std::holds_alternative<Vector<T>>(get(n))
                   ? VectorView<T>{get<Vector<T>>(n)}
                   : get<VectorView<T>>(n);
    }

    std::size_t numResults() const { return m_numResults; }

private:
    std::array<Data<T>, 4> m_data;
    std::size_t m_numResults = 0;

    void assertN(std::size_t n) const
    {
        if(n >= m_numResults)
            throw std::out_of_range{"The index `n` exceeds the number of function results."};
    }
};


auto missingImplException(auto* self)
{
    return std::runtime_error{std::string{"No call operator implementation of '"} + self->name()
                              + "' available that can be applied to the passed argument(s)."};
}

// Function types
// ======================================================

template <ArithmeticType T = DefaultArithmeticType>
struct AbstractUnaryFunction
{
    static constexpr auto arity = 1;

    virtual const char* name() const = 0;

    virtual FunctionResult<T> operator()(Number<T>)     const { throw missingImplException(this); }
    virtual FunctionResult<T> operator()(Vector<T>&& v) const { return (*this)(v); }
    virtual FunctionResult<T> operator()(VectorView<T>) const { throw missingImplException(this); }

    template <typename Variant>
        requires std::same_as<Variant, Data<T>>
    FunctionResult<T> operator()(Variant&& variant) const
    {
        return std::visit([this]<typename D>(D&& data) { return (*this)(std::forward<D>(data)); },
                          std::forward<Variant>(variant));
    }

    virtual ~AbstractUnaryFunction() = default;
};

template <ArithmeticType T = DefaultArithmeticType>
using UnaryFunction = PolymorphicFunctor<AbstractUnaryFunction<T>>;


template <ArithmeticType T = DefaultArithmeticType>
struct AbstractBinaryFunction
{
    static constexpr auto arity = 2;

    virtual const char* name() const = 0;

    virtual FunctionResult<T> operator()(Number<T>,      Number<T>)      const { throw missingImplException(this); }
    virtual FunctionResult<T> operator()(Number<T>,      VectorView<T>)  const { throw missingImplException(this); }
    virtual FunctionResult<T> operator()(Number<T> n,    Vector<T>&& v)  const { return (*this)(n, v); }
    virtual FunctionResult<T> operator()(VectorView<T>,  Number<T>)      const { throw missingImplException(this); }
    virtual FunctionResult<T> operator()(Vector<T>&& v,  Number<T> n)    const { return (*this)(v, n); }
    virtual FunctionResult<T> operator()(VectorView<T>,  VectorView<T>)  const { throw missingImplException(this); }
    virtual FunctionResult<T> operator()(Vector<T>&& v1, Vector<T>&& v2) const { return (*this)(v1, v2); }

    template <typename Variant>
        requires std::same_as<Variant, Data<T>>
    FunctionResult<T> operator()(Variant&& variant1, Variant&& variant2) const
    {
        return std::visit([this]<typename D1, typename D2>(D1&& data1, D2&& data2) { return (*this)(std::forward<D1>(data1),
                                                                                                    std::forward<D2>(data2)); },
                          std::forward<Variant>(variant1),
                          std::forward<Variant>(variant2));
    }

    virtual ~AbstractBinaryFunction() = default;
};

template <ArithmeticType T = DefaultArithmeticType>
using BinaryFunction = PolymorphicFunctor<AbstractBinaryFunction<T>>;


template <ArithmeticType T = DefaultArithmeticType>
using Function = std::variant<UnaryFunction<T>, BinaryFunction<T>>;


// Combinator types
// ======================================================

template <ArithmeticType T = DefaultArithmeticType>
struct AbstractUnaryCombinator
{
    static constexpr auto arity = 1;

    virtual const char* name() const = 0;

    virtual Function<T> operator()(const UnaryFunction<T>&)  const { throw missingImplException(this); }
    virtual Function<T> operator()(const BinaryFunction<T>&) const { throw missingImplException(this); }

    template <typename Variant>
        requires std::same_as<Variant, Function<T>>
    Function<T> operator()(const Variant& variant) const
    {
        return std::visit([this](const auto& fun) { return (*this)(fun); }, variant);
    }

    virtual ~AbstractUnaryCombinator() = default;
};

template <ArithmeticType T = DefaultArithmeticType>
using UnaryCombinator = PolymorphicFunctor<AbstractUnaryCombinator<T>>;


template <ArithmeticType T = DefaultArithmeticType>
struct AbstractBinaryCombinator
{
    static constexpr auto arity = 2;

    virtual const char* name() const = 0;

    virtual Function<T> operator()(const UnaryFunction<T>&,  const UnaryFunction<T>&)  const { throw missingImplException(this); }
    virtual Function<T> operator()(const UnaryFunction<T>&,  const BinaryFunction<T>&) const { throw missingImplException(this); }
    virtual Function<T> operator()(const BinaryFunction<T>&, const UnaryFunction<T>&)  const { throw missingImplException(this); }
    virtual Function<T> operator()(const BinaryFunction<T>&, const BinaryFunction<T>&) const { throw missingImplException(this); }

    template <typename Variant>
        requires std::same_as<Variant, Function<T>>
    Function<T> operator()(const Variant& variant1, const Variant& variant2) const
    {
        return std::visit([this](const auto& fun1, const auto& fun2) { return (*this)(fun1, fun2); }, variant1, variant2);
    }

    virtual ~AbstractBinaryCombinator() = default;
};

template <ArithmeticType T = DefaultArithmeticType>
using BinaryCombinator = PolymorphicFunctor<AbstractBinaryCombinator<T>>;


template <ArithmeticType T = DefaultArithmeticType>
using Combinator = std::variant<UnaryCombinator<T>, BinaryCombinator<T>>;


// The generic vocabular element
// ======================================================

template <typename T>
inline constexpr bool alwaysFalseValue = false;

template <ArithmeticType T = DefaultArithmeticType>
using Element = std::variant<Data<T>, Function<T>, Combinator<T>>;

template <ArithmeticType T>
struct Train
{
    std::vector<Element<T>> elements;

    template <typename... Es>
        requires(!std::same_as<std::remove_cvref_t<Es>, Train<T>> && ...)
    Train(Es&&... concreteElements)
    {
        elements.reserve(sizeof...(concreteElements));
        (add(std::forward<Es>(concreteElements)), ...);
    }

    template <typename E>
    void add(E&& concreteElement)
    {
        if constexpr(std::constructible_from<Element<T>, E>)
            elements.emplace_back(std::forward<E>(concreteElement));
        else if constexpr(std::constructible_from<Combinator<T>, E>)
            elements.emplace_back(Combinator<T>{std::forward<E>(concreteElement)});
        else if constexpr(std::constructible_from<Function<T>, E>)
            elements.emplace_back(Function<T>{std::forward<E>(concreteElement)});
        else if constexpr(std::derived_from<std::remove_cvref_t<E>, AbstractUnaryCombinator<T>>)
            elements.emplace_back(Combinator<T>{UnaryCombinator<T>{std::forward<E>(concreteElement)}});
        else if constexpr(std::derived_from<std::remove_cvref_t<E>, AbstractBinaryCombinator<T>>)
            elements.emplace_back(Combinator<T>{BinaryCombinator<T>{std::forward<E>(concreteElement)}});
        else if constexpr(std::derived_from<std::remove_cvref_t<E>, AbstractUnaryFunction<T>>)
            elements.emplace_back(Function<T>{UnaryFunction<T>{std::forward<E>(concreteElement)}});
        else if constexpr(std::derived_from<std::remove_cvref_t<E>, AbstractBinaryFunction<T>>)
            elements.emplace_back(Function<T>{BinaryFunction<T>{std::forward<E>(concreteElement)}});
        else if constexpr(std::constructible_from<Data<T>, E>)
            elements.emplace_back(Data<T>{std::forward<E>(concreteElement)});
        else if constexpr(std::constructible_from<VectorView<T>, E>)
            elements.emplace_back(Data<T>{VectorView<T>{std::forward<E>(concreteElement)}});
        else
            static_assert(alwaysFalseValue<E>, "Incompatible element type.");
    }
};

// Template machinery for the deduction guide: deduce value type from the first constructur argument
template <typename E>
struct ExtractValueType { using type = E; };

template <template<typename> class E, typename T>
struct ExtractValueType<E<T>> { using type = std::remove_const_t<T>; };

template <template<typename, std::size_t> class E, typename T, std::size_t S>
struct ExtractValueType<E<T, S>> { using type = std::remove_const_t<T>; };

template <template<typename, typename> class E, typename T, typename Alloc>
struct ExtractValueType<E<T, Alloc>> { using type = std::remove_const_t<T>; };

template <typename... Es>
struct FirstValueType { using type = ExtractValueType<std::tuple_element_t<0, std::tuple<Es...>>>::type; };

template <typename... Es> Train(Es...) -> Train<typename FirstValueType<Es...>::type>;

// Auxilary function
template <ArithmeticType T>
Train<T> cat(Train<T> t1, const Train<T>& t2)
{
    t1.elements.insert(end(t1.elements), begin(t2.elements), end(t2.elements));
    return t1;
}

// Concrete Functions
// ======================================================

// Unary Functions
template <ArithmeticType T = DefaultArithmeticType>
struct Identity : AbstractUnaryFunction<T>
{
    const char* name() const override { return "Identity"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> n)     const override { return {n}; }
    FunctionResult<T> operator()(Vector<T>&& v)   const override { return {std::move(v)}; }
    FunctionResult<T> operator()(VectorView<T> v) const override { return {Vector<T>{begin(v), end(v)}}; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Duplicate : AbstractUnaryFunction<T>
{
    const char* name() const override { return "Duplicate"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> n) const override { return {n, n}; }
    FunctionResult<T> operator()(Vector<T>&& v) const override
    {
        auto cpy = v;
        return {std::move(v), std::move(cpy)};
    }
    FunctionResult<T> operator()(VectorView<T> v) const override
    {
        return {Vector<T>{begin(v), end(v)}, Vector<T>{begin(v), end(v)}};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Pop : AbstractUnaryFunction<T>
{
    const char* name() const override { return "Pop"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T>)     const override { return {}; }
    FunctionResult<T> operator()(VectorView<T>) const override { return {}; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Length : AbstractUnaryFunction<T>
{
    const char* name() const override { return "Length"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T>)       const override { return {Number<T>{1}}; }
    FunctionResult<T> operator()(VectorView<T> v) const override { return {static_cast<Number<T>>(v.size())}; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Range : AbstractUnaryFunction<T>
{
    const char* name() const override { return "Range"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> size) const override
    {
        if(std::signbit(static_cast<double>(size)))
            throw std::runtime_error{"The number of Range elements must be non-negative."};

        auto ret = Vector<T>(static_cast<std::size_t>(size));
        std::iota(begin(ret), end(ret), T(0));
        return {std::move(ret)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Reverse : AbstractUnaryFunction<T>
{
    const char* name() const override { return "Reverse"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> n) const override { return {n}; }
    FunctionResult<T> operator()(VectorView<T> v) const override
    {
        auto ret = Vector<T>(size(v));
        std::ranges::reverse_copy(v, begin(ret));
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& v) const override
    {
        std::ranges::reverse(v);
        return {std::move(v)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct MaxElement : AbstractUnaryFunction<T>
{
    const char* name() const override { return "MaxElement"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> n)     const override { return {n}; }
    FunctionResult<T> operator()(VectorView<T> v) const override { return {std::ranges::max(v)}; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct MinElement : AbstractUnaryFunction<T>
{
    const char* name() const override { return "MinElement"; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> n)     const override { return {n}; }
    FunctionResult<T> operator()(VectorView<T> v) const override { return {std::ranges::min(v)}; }
};

template <auto UnaryCallable, ArithmeticType T = DefaultArithmeticType>
    requires requires(T val) { {UnaryCallable(val)} -> std::same_as<T>; }
struct AbstractUnaryOp : AbstractUnaryFunction<T>
{
    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> n) const override { return {UnaryCallable(n)}; }
    FunctionResult<T> operator()(Vector<T>&& v) const override
    {
        std::ranges::transform(v, begin(v), [](auto val) { return UnaryCallable(val); });
        return {std::move(v)};
    }
    FunctionResult<T> operator()(VectorView<T> v) const override
    {
        auto ret = Vector<T>(size(v));
        std::ranges::transform(v, begin(ret), [](auto val) { return UnaryCallable(val); });
        return {std::move(ret)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Negate : AbstractUnaryOp<[](T val) { return -val; }, T>
{
    const char* name() const override { return "Negate"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Abs : AbstractUnaryOp<[](T val) { return T(std::abs(val)); }, T>
{
    const char* name() const override { return "Abs"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Sqrt : AbstractUnaryOp<[](T val) { return T(std::sqrt(val)); }, T>
{
    const char* name() const override { return "Sqrt"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Exp : AbstractUnaryOp<[](T val) { return T(std::exp(val)); }, T>
{
    const char* name() const override { return "Exp"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Log : AbstractUnaryOp<[](T val) { return T(std::log(val)); }, T>
{
    const char* name() const override { return "Log"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Floor : AbstractUnaryOp<[](T val) { return T(std::floor(val)); }, T>
{
    const char* name() const override { return "Floor"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Ceil : AbstractUnaryOp<[](T val) { return T(std::ceil(val)); }, T>
{
    const char* name() const override { return "Ceil"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Round : AbstractUnaryOp<[](T val) { return T(std::round(val)); }, T>
{
    const char* name() const override { return "Round"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Sin : AbstractUnaryOp<[](T val) { return T(std::sin(val)); }, T>
{
    const char* name() const override { return "Sin"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Cos : AbstractUnaryOp<[](T val) { return T(std::cos(val)); }, T>
{
    const char* name() const override { return "Cos"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Tan : AbstractUnaryOp<[](T val) { return T(std::tan(val)); }, T>
{
    const char* name() const override { return "Tan"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct ASin : AbstractUnaryOp<[](T val) { return T(std::asin(val)); }, T>
{
    const char* name() const override { return "ASin"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct ACos : AbstractUnaryOp<[](T val) { return T(std::acos(val)); }, T>
{
    const char* name() const override { return "ACos"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct ATan : AbstractUnaryOp<[](T val) { return T(std::atan(val)); }, T>
{
    const char* name() const override { return "ATan"; }
};

// Binary Functions
template <ArithmeticType T = DefaultArithmeticType>
struct Flip : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Flip"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> a, Number<T> b) const override
    {
        return {b, a};
    }
    FunctionResult<T> operator()(Number<T> a, VectorView<T> b) const override
    {
        return {Vector<T>(begin(b), end(b)), a};
    }
    FunctionResult<T> operator()(Number<T> a, Vector<T>&& b) const override
    {
        return {std::move(b), a};
    }
    FunctionResult<T> operator()(VectorView<T> a, Number<T> b) const override
    {
        return {b, Vector<T>(begin(a), end(a))};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Number<T> b) const override
    {
        return {b, std::move(a)};
    }
    FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override
    {
        return {Vector<T>(begin(b), end(b)), Vector<T>(begin(a), end(a))};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Vector<T>&& b) const override
    {
        return {std::move(b), std::move(a)};
    }
};

// Binary Functions
template <ArithmeticType T = DefaultArithmeticType>
struct Join : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Join"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> a, Number<T> b) const override
    {
        return {Vector<T>{b, a}};
    }
    FunctionResult<T> operator()(Number<T> a, VectorView<T> b) const override
    {
        auto ret = Vector<T>(size(b) + 1);
        std::ranges::copy(b, begin(ret));
        ret.back() = a;
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(VectorView<T> a, Number<T> b) const override
    {
        auto ret = Vector<T>(size(a) + 1);
        ret.front() = b;
        std::ranges::copy(a, next(begin(ret)));
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override
    {
        auto ret = Vector<T>(size(a) + size(b));
        std::ranges::copy(a,
                          std::ranges::copy(b, begin(ret)).out);
        return {std::move(ret)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Over : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Over"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> a, Number<T> b) const override
    {
        return {a, b, a};
    }
    FunctionResult<T> operator()(Number<T> a, VectorView<T> b) const override
    {
        return {a, Vector<T>(begin(b), end(b)), a};
    }
    FunctionResult<T> operator()(Number<T> a, Vector<T>&& b) const override
    {
        return {a, std::move(b), a};
    }
    FunctionResult<T> operator()(VectorView<T> a, Number<T> b) const override
    {
        return {Vector<T>(begin(a), end(a)), b, Vector<T>(begin(a), end(a))};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Number<T> b) const override
    {
        return {std::move(a), b, std::move(a)};
    }
    FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override
    {
        return {Vector<T>(begin(a), end(a)), Vector<T>(begin(b), end(b)), Vector<T>(begin(a), end(a))};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Vector<T>&& b) const override
    {
        return {std::move(a), std::move(b), std::move(a)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Drop : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Drop"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(VectorView<T> v, Number<T> n) const override
    {
        const auto numToDrop = getNumToDrop(v, n);

        auto ret = Vector<T>(size(v) - numToDrop);
        std::copy(next(begin(v), numToDrop), end(v), begin(ret));
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& v, Number<T> n) const override
    {
        v.erase(begin(v), next(begin(v), getNumToDrop(v, n)));
        return {std::move(v)};
    }

private:
    static std::size_t getNumToDrop(VectorView<T> v, T n)
    {
        if(std::signbit(static_cast<double>(n)))
            throw std::runtime_error{"The number of elements to be dropped must be non-negative."};

        const auto ret = static_cast<std::size_t>(n);
        if(ret > size(v))
            throw std::runtime_error{"Cannot drop more elements than the size of the vector."};

        return ret;
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Take : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Take"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(VectorView<T> v, Number<T> n) const override
    {
        const auto numToTake = getNumToTake(v, n);

        auto ret = Vector<T>(numToTake);
        std::copy_n(begin(v), numToTake, begin(ret));
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& v, Number<T> n) const override
    {
        v.resize(getNumToTake(v, n));
        return {std::move(v)};
    }

private:
    static std::size_t getNumToTake(VectorView<T> v, T n)
    {
        if(std::signbit(static_cast<double>(n)))
            throw std::runtime_error{"The number of elements to be taken must be non-negative."};

        const auto ret = static_cast<std::size_t>(n);
        if(ret > size(v))
            throw std::runtime_error{"Cannot take more elements than the size of the vector."};

        return ret;
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Find : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Find"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> a, Number<T> b) const override
    {
        return {T(a == b)};
    }
    FunctionResult<T> operator()(Number<T>, VectorView<T>) const override
    {
        return {T(0)};
    }
    FunctionResult<T> operator()(VectorView<T> a, Number<T> b) const override
    {
        auto ret = Vector<T>(size(a));
        std::ranges::transform(a, begin(ret), [b](auto val) { return T(val == b); });
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Number<T> b) const override
    {
        std::ranges::transform(a, begin(a), [b](auto val) { return T(val == b); });
        return {std::move(a)};
    }
    FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override
    {
        auto ret = Vector<T>(size(a), T(0));

        if(empty(b) || size(b) > size(a))
            return {std::move(ret)};

        for(auto it = begin(a); ;)
        {
            it = std::search(it, end(a), begin(b), end(b));
            if(it == end(a))
                break;
            ret[std::distance(begin(a), it++)] = T(1);
        }

        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Vector<T>&& b) const override
    {
        if(empty(b) || size(b) > size(a))
            return {Vector<T>(size(a), T(0))};

        for(auto it = begin(a); ;)
        {
            const auto foundIt = std::search(it, end(a), begin(b), end(b));
            std::fill(it, foundIt, T(0));
            if(foundIt == end(a))
                break;
            a[std::distance(begin(a), foundIt)] = T(1);
            it = next(foundIt);
        }

        return {std::move(a)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Keep : AbstractBinaryFunction<T>
{
    const char* name() const override { return "Keep"; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> a, Number<T> b) const override
    {
        return {Vector<T>(getNumOfReplications(b), a)};
    }
    FunctionResult<T> operator()(VectorView<T> a, Number<T> b) const override
    {
        const auto numOfReplications = getNumOfReplications(b);
        auto ret = Vector<T>(numOfReplications * size(a));
        auto outIt = begin(ret);
        for(auto i = 0u; i < numOfReplications; ++i)
            outIt = std::ranges::copy(a, outIt).out;
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Number<T> b) const override
    {
        if(getNumOfReplications(b) == 1u)
            return {std::move(a)};

        return (*this)(a, b);
    }
    FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override
    {
        const auto inSize = size(a);
        if(inSize != size(b))
            throw std::runtime_error{"Cannot use Keep with Vectors of different length."};

        const auto outSize = std::transform_reduce(begin(b), end(b), std::size_t{},
                                                   std::plus{},
                                                   &Keep::getNumOfReplications);

        auto ret = Vector<T>{};
        ret.reserve(outSize);

        for(std::size_t i = 0, numFilled = 0; i < inSize; ++i)
            ret.resize(numFilled += static_cast<std::size_t>(b[i]), a[i]);

        return {std::move(ret)};
    }

private:
    static std::size_t getNumOfReplications(T n)
    {
        if(std::signbit(static_cast<double>(n)))
            throw std::runtime_error{"The number of replications must be non-negative."};

        return static_cast<std::size_t>(n);
    }
};

template <auto BinaryCallable, ArithmeticType T = DefaultArithmeticType>
    requires requires(T val1, T val2) { {BinaryCallable(val1, val2)} -> std::same_as<T>; }
struct AbstractBinaryOp : AbstractBinaryFunction<T>
{
    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> a, Number<T> b) const override { return {BinaryCallable(a, b)}; }
    FunctionResult<T> operator()(Number<T> a, VectorView<T> b) const override
    {
        auto ret = Vector<T>(size(b));
        std::ranges::transform(b, begin(ret), [a](auto val) { return BinaryCallable(a, val); });
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Number<T> a, Vector<T>&& b) const override
    {
        std::ranges::transform(b, begin(b), [a](auto val) { return BinaryCallable(a, val); });
        return {std::move(b)};
    }
    FunctionResult<T> operator()(VectorView<T> a, Number<T> b) const override
    {
        auto ret = Vector<T>(size(a));
        std::ranges::transform(a, begin(ret), [b](auto val) { return BinaryCallable(val, b); });
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Number<T> b) const override
    {
        std::ranges::transform(a, begin(a), [b](auto val) { return BinaryCallable(val, b); });
        return {std::move(a)};
    }
    FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override
    {
        auto ret = Vector<T>(size(a));
        std::ranges::transform(a, b, begin(ret), BinaryCallable);
        return {std::move(ret)};
    }
    FunctionResult<T> operator()(Vector<T>&& a, Vector<T>&& b) const override
    {
        std::ranges::transform(a, b, begin(a), BinaryCallable);
        return {std::move(a)};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Equal : AbstractBinaryOp<[](T a, T b) { return T(a == b); }, T>
{
    const char* name() const override { return "Equal"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct NotEqual : AbstractBinaryOp<[](T a, T b) { return T(a != b); }, T>
{
    const char* name() const override { return "NotEqual"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Less : AbstractBinaryOp<[](T a, T b) { return T(a < b); }, T>
{
    const char* name() const override { return "Less"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct LessEqual : AbstractBinaryOp<[](T a, T b) { return T(a <= b); }, T>
{
    const char* name() const override { return "LessEqual"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Greater : AbstractBinaryOp<[](T a, T b) { return T(a > b); }, T>
{
    const char* name() const override { return "Greater"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct GreaterEqual : AbstractBinaryOp<[](T a, T b) { return T(a >= b); }, T>
{
    const char* name() const override { return "GreaterEqual"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Min : AbstractBinaryOp<[](T a, T b) { return std::min(a, b); }, T>
{
    const char* name() const override { return "Min"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Max : AbstractBinaryOp<[](T a, T b) { return std::max(a, b); }, T>
{
    const char* name() const override { return "Max"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Plus : AbstractBinaryOp<std::plus<T>{}, T>
{
    const char* name() const override { return "Plus"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Multiplies : AbstractBinaryOp<std::multiplies<T>{}, T>
{
    const char* name() const override { return "Multiplies"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Minus : AbstractBinaryOp<std::minus<T>{}, T>
{
    const char* name() const override { return "Minus"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Divides : AbstractBinaryOp<std::divides<T>{}, T>
{
    const char* name() const override { return "Divides"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Modulus : AbstractBinaryOp<std::modulus<T>{}, T>
{
    const char* name() const override { return "Modulus"; }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Pow : AbstractBinaryOp<[](T a, T b) { return T(std::pow(a, b)); }, T>
{
    const char* name() const override { return "Power"; }
};


// Concrete Combinators
// ======================================================

template <ArithmeticType T = DefaultArithmeticType>
struct Reduction : AbstractUnaryCombinator<T>
{
    const char* name() const override { return "Reduction"; }

    using AbstractUnaryCombinator<T>::operator();

    Function<T> operator()(const BinaryFunction<T>& f) const override
    {
        struct Reduce : AbstractUnaryFunction<T>
        {
            Reduce(const BinaryFunction<T>& fun) : m_fun{fun} {}

            const char* name() const override { return "Reduce"; }

            using AbstractUnaryFunction<T>::operator();

            FunctionResult<T> operator()(Number<T> n) const override
            {
                return {n};
            }
            FunctionResult<T> operator()(VectorView<T> v) const override
            {
                if(empty(v))
                    return {T{}};

                return {std::reduce(next(begin(v)), end(v), v.front(), [this](auto a, auto b) {
                    return m_fun(a, b).getNumber(0);
                })};
            }

        private:
            BinaryFunction<T> m_fun;
        };

        return UnaryFunction<T>{Reduce{f}};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Scan : AbstractUnaryCombinator<T>
{
    const char* name() const override { return "Scan"; }

    using AbstractUnaryCombinator<T>::operator();

    Function<T> operator()(const BinaryFunction<T>& f) const override
    {
        struct ApplyScan : AbstractUnaryFunction<T>
        {
            ApplyScan(const BinaryFunction<T>& fun) : m_fun{fun} {}

            const char* name() const override { return "ApplyScan"; }

            using AbstractUnaryFunction<T>::operator();

            FunctionResult<T> operator()(VectorView<T> v) const override
            {
                if(empty(v))
                    return {Vector<T>{}};

                auto ret = Vector<T>(size(v));
                std::inclusive_scan(begin(v), end(v), begin(ret), [this](auto a, auto b) {
                    return m_fun(a, b).getNumber(0);
                });
                return {std::move(ret)};
            }
            FunctionResult<T> operator()(Vector<T>&& v) const override
            {
                if(empty(v))
                    return {Vector<T>{}};

                std::inclusive_scan(begin(v), end(v), begin(v), [this](auto a, auto b) {
                    return m_fun(a, b).getNumber(0);
                });
                return {std::move(v)};
            }

        private:
            BinaryFunction<T> m_fun;
        };

        return UnaryFunction<T>{ApplyScan{f}};
    }
};

template <ArithmeticType T = DefaultArithmeticType>
struct Both : AbstractUnaryCombinator<T>
{
    const char* name() const override { return "Both"; }

    using AbstractUnaryCombinator<T>::operator();

    Function<T> operator()(const UnaryFunction<T>& f) const override
    {
        struct UseOnBoth : AbstractBinaryFunction<T>
        {
            UseOnBoth(const UnaryFunction<T>& fun) : m_fun{fun} {}

            const char* name() const override { return "UseOnBoth"; }

            using AbstractBinaryFunction<T>::operator();

            FunctionResult<T> operator()(Number<T> a, Number<T> b)         const override { return combineResults(m_fun(a), m_fun(b)); }
            FunctionResult<T> operator()(Number<T> a, VectorView<T> b)     const override { return combineResults(m_fun(a), m_fun(b)); }
            FunctionResult<T> operator()(Number<T> a, Vector<T>&& b)       const override { return combineResults(m_fun(a), m_fun(std::move(b))); }
            FunctionResult<T> operator()(VectorView<T> a, Number<T> b)     const override { return combineResults(m_fun(a), m_fun(b)); }
            FunctionResult<T> operator()(Vector<T>&& a, Number<T> b)       const override { return combineResults(m_fun(std::move(a)), m_fun(b)); }
            FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override { return combineResults(m_fun(a), m_fun(b)); }
            FunctionResult<T> operator()(Vector<T>&& a, Vector<T>&& b)     const override { return combineResults(m_fun(std::move(a)), m_fun(std::move(b))); }

        private:
            UnaryFunction<T> m_fun;
        };

        return BinaryFunction<T>{UseOnBoth{f}};
    }

private:
    static FunctionResult<T> combineResults(FunctionResult<T>&& res1, FunctionResult<T>&& res2)
    {
        for(auto i = 0u; i < res2.numResults(); ++i)
            res1.add(std::move(res2.get(i)));

        return std::move(res1);
    }
};


template <ArithmeticType T = DefaultArithmeticType>
struct Fork : AbstractBinaryCombinator<T>
{
    const char* name() const override { return "Fork"; }

    using AbstractBinaryCombinator<T>::operator();

    Function<T> operator()(const UnaryFunction<T>& f1,  const UnaryFunction<T>& f2) const override
    {
        struct Bifurcate : AbstractUnaryFunction<T>
        {
            Bifurcate(const UnaryFunction<T>& fun1, const UnaryFunction<T>& fun2) : m_fun1{fun1}, m_fun2{fun2} {}

            const char* name() const override { return "Bifurcate(u, u)"; }

            using AbstractUnaryFunction<T>::operator();

            FunctionResult<T> operator()(Number<T> n)     const override { return combineResults(m_fun1(n), m_fun2(n)); }
            FunctionResult<T> operator()(VectorView<T> v) const override { return combineResults(m_fun1(v), m_fun2(v)); }

        private:
            UnaryFunction<T> m_fun1;
            UnaryFunction<T> m_fun2;
        };

        return UnaryFunction<T>{Bifurcate{f1, f2}};
    }

    Function<T> operator()(const UnaryFunction<T>& f1,  const BinaryFunction<T>& f2) const override
    {
        struct Bifurcate : AbstractBinaryFunction<T>
        {
            Bifurcate(const UnaryFunction<T>& fun1, const BinaryFunction<T>& fun2) : m_fun1{fun1}, m_fun2{fun2} {}

            const char* name() const override { return "Bifurcate(u, b)"; }

            using AbstractBinaryFunction<T>::operator();

            FunctionResult<T> operator()(Number<T> a, Number<T> b)         const override { return combineResults(m_fun1(a), m_fun2(a, b)); }
            FunctionResult<T> operator()(Number<T> a, VectorView<T> b)     const override { return combineResults(m_fun1(a), m_fun2(a, b)); }
            FunctionResult<T> operator()(VectorView<T> a, Number<T> b)     const override { return combineResults(m_fun1(a), m_fun2(a, b)); }
            FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override { return combineResults(m_fun1(a), m_fun2(a, b)); }

        private:
            UnaryFunction<T>  m_fun1;
            BinaryFunction<T> m_fun2;
        };

        return BinaryFunction<T>{Bifurcate{f1, f2}};
    }

    Function<T> operator()(const BinaryFunction<T>& f1,  const UnaryFunction<T>& f2) const override
    {
        struct Bifurcate : AbstractBinaryFunction<T>
        {
            Bifurcate(const BinaryFunction<T>& fun1, const UnaryFunction<T>& fun2) : m_fun1{fun1}, m_fun2{fun2} {}

            const char* name() const override { return "Bifurcate(b, u)"; }

            using AbstractBinaryFunction<T>::operator();

            FunctionResult<T> operator()(Number<T> a, Number<T> b)         const override { return combineResults(m_fun1(a, b), m_fun2(a)); }
            FunctionResult<T> operator()(Number<T> a, VectorView<T> b)     const override { return combineResults(m_fun1(a, b), m_fun2(a)); }
            FunctionResult<T> operator()(VectorView<T> a, Number<T> b)     const override { return combineResults(m_fun1(a, b), m_fun2(a)); }
            FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override { return combineResults(m_fun1(a, b), m_fun2(a)); }

        private:
            BinaryFunction<T> m_fun1;
            UnaryFunction<T>  m_fun2;
        };

        return BinaryFunction<T>{Bifurcate{f1, f2}};
    }

    Function<T> operator()(const BinaryFunction<T>& f1,  const BinaryFunction<T>& f2) const override
    {
        struct Bifurcate : AbstractBinaryFunction<T>
        {
            Bifurcate(const BinaryFunction<T>& fun1, const BinaryFunction<T>& fun2) : m_fun1{fun1}, m_fun2{fun2} {}

            const char* name() const override { return "Bifurcate(b, b)"; }

            using AbstractBinaryFunction<T>::operator();

            FunctionResult<T> operator()(Number<T> a, Number<T> b)         const override { return combineResults(m_fun1(a, b), m_fun2(a, b)); }
            FunctionResult<T> operator()(Number<T> a, VectorView<T> b)     const override { return combineResults(m_fun1(a, b), m_fun2(a, b)); }
            FunctionResult<T> operator()(VectorView<T> a, Number<T> b)     const override { return combineResults(m_fun1(a, b), m_fun2(a, b)); }
            FunctionResult<T> operator()(VectorView<T> a, VectorView<T> b) const override { return combineResults(m_fun1(a, b), m_fun2(a, b)); }

        private:
            BinaryFunction<T> m_fun1;
            BinaryFunction<T> m_fun2;
        };

        return BinaryFunction<T>{Bifurcate{f1, f2}};
    }

private:
    static FunctionResult<T> combineResults(FunctionResult<T>&& res1, FunctionResult<T>&& res2)
    {
        for(auto i = 0u; i < res1.numResults(); ++i)
            res2.add(std::move(res1.get(i)));

        return std::move(res2);
    }
};


// Helper functions
// ======================================================

int getArity(const auto& functorVariant)
{
    return std::visit([](const auto& f) { return f->arity; }, functorVariant);
}

template <ArithmeticType T>
int getArity(const Element<T>& element)
{
    return std::visit(Overload{[](const Data<T>&) { return 0; },
                               [](const auto& e)  { return getArity(e); }},
                      element);
}

const char* getName(const auto& functorVariant)
{
    return std::visit([](const auto& f) { return f->name(); }, functorVariant);
}

template <ArithmeticType T>
const char* getName(const Data<T>& data)
{
    return std::visit(Overload{[](const Number<T>&)     { return "Number"; },
                               [](const Vector<T>&)     { return "Vector"; },
                               [](const VectorView<T>&) { return "VectorView"; }},
                      data);
}

template <ArithmeticType T>
const char* getName(const Element<T>& element)
{
    return std::visit(Overload{[](const auto& e) { return getName(e); }}, element);
}

template <ArithmeticType T>
void applyCombinatorPass(Train<T>& train)
{
    auto& elements = train.elements;
    auto it = elements.rbegin();
    while(it != elements.rend())
    {
        if(!std::holds_alternative<Combinator<T>>(*it))
        {
            ++it;
            continue;
        }

        const auto forw = [](auto revIt)  { return next(revIt).base(); };
        const auto rev  = [](auto forwIt) { return std::reverse_iterator{next(forwIt)}; };

        const auto c = std::get<Combinator<T>>(*it);
        it = rev(elements.erase(forw(it)));
        const auto arity = getArity(c);
        if(arity > std::distance(forw(it), elements.end()))
            throw std::runtime_error{"missing function object"};

        switch(arity)
        {
            break; case 1:
        {
            auto f = std::get<Function<T>>(*it);
            auto forwIt = elements.erase(forw(it));
            it = rev(elements.insert(forwIt, std::get<UnaryCombinator<T>>(c)(std::move(f))));
        }
        break; case 2:
        {
            auto f1 = std::get<Function<T>>(*it);
            auto f2 = std::get<Function<T>>(*prev(it));
            auto forwIt = elements.erase(forw(it), next(forw(it), 2));
            it = rev(elements.insert(forwIt, std::get<BinaryCombinator<T>>(c)(std::move(f1),
                                                                              std::move(f2))));
        }
        break; default: throw std::runtime_error{"invalid combinator arity"};
        }
    }
}

template <ArithmeticType T>
std::vector<Data<T>> applyFunctionPass(const Train<T>& train)
{
    auto ret = std::vector<Data<T>>{};

    for(auto it = train.elements.rbegin(); it != train.elements.rend(); ++it)
    {
        if(std::holds_alternative<Combinator<T>>(*it))
        {
            throw std::runtime_error{"Combinators are not allowed in a Function pass. "
                                     "Apply the Combinator pass beforehand!"};
        }

        if(std::holds_alternative<Data<T>>(*it))
        {
            //std::cout << "push data - ";
            ret.push_back(std::move(std::get<Data<T>>(*it)));
            continue;
        }

        const auto f = std::get<Function<T>>(*it);
        //std::cout << getName(f) << " - ";

        const auto arity = getArity(f);
        if(std::cmp_greater(arity, ret.size()))
            throw std::runtime_error{"Data stack size: " + std::to_string(ret.size())
                                     + ", but arity of " + getName(f) + " is " + std::to_string(arity) + '.'};

        FunctionResult<T> res;
        switch(arity)
        {
            break; case 1:
        {
            res = std::get<UnaryFunction<T>>(f)(std::move(ret.back()));
            ret.pop_back();
        }
        break; case 2:
        {
            res = std::get<BinaryFunction<T>>(f)(std::move(*prev(ret.end(), 2)),
                                                 std::move(*prev(ret.end(), 1)));
            ret.pop_back();
            ret.pop_back();
        }
        break; default: throw std::runtime_error{"invalid function arity"};
        }

        for(std::size_t i = 0; i < res.numResults(); ++i)
            ret.push_back(std::move(res.get(i)));
    }

    return ret;
}

template <ArithmeticType T>
void print(const Train<T>& train)
{
    for(const auto& e : train.elements)
        std::cout << getName(e) << "_a" << getArity(e) << ' ';
    std::cout << '\n';
}


template <ArithmeticType T>
void print(const VectorView<T>& v)
{
    std::cout << "[ ";
    for(const auto& e : v) std::cout << e << ' ';
    std::cout << "]\n";
}

template <ArithmeticType T>
void print(const Data<T>& data)
{
    std::visit(Overload{[](Number<T> n) { std::cout << n << '\n'; },
                        [](VectorView<T> v) { print(v); }},
               data);
}

template <typename DataType>
void print(const std::vector<DataType>& dataStack)
{
    for(const auto& data : dataStack)
    {
        print(data);
    }
}

template <ArithmeticType T>
void print(const FunctionResult<T>& res)
{
    for(auto i = 0u; i < res.numResults(); ++i)
    {
        print(res.get(i));
    }
}

// Special Functions generated from Trains
// ======================================================

template <ArithmeticType T>
struct UnaryFunctionFromTrain : AbstractUnaryFunction<T>
{
    UnaryFunctionFromTrain(Train<T> t) : m_train{std::move(t)} { applyCombinatorPass(m_train); }

    const char* name() const override { return "UnaryFunctionFromTrain"; }

    const Train<T>& train() const { return m_train; }

    using AbstractUnaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> d)     const override { return invoke_impl(std::move(d)); }
    FunctionResult<T> operator()(Vector<T>&& d)   const override { return invoke_impl(std::move(d)); }
    FunctionResult<T> operator()(VectorView<T> d) const override { return invoke_impl(std::move(d)); }

    FunctionResult<T> invoke_impl(Data<T>&& data) const
    {
        m_train.add(std::move(data));
        auto ret = FunctionResult<T>::fromDataVector(applyFunctionPass(m_train));
        m_train.elements.pop_back(); // train restored
        return ret;
    }

private:
    mutable Train<T> m_train;
};

template <ArithmeticType T>
struct BinaryFunctionFromTrain : AbstractBinaryFunction<T>
{
    BinaryFunctionFromTrain(Train<T> t) : m_train{std::move(t)} { applyCombinatorPass(m_train); }

    const char* name() const override { return "BinaryFunctionFromTrain"; }

    const Train<T>& train() const { return m_train; }

    using AbstractBinaryFunction<T>::operator();

    FunctionResult<T> operator()(Number<T> d1,     Number<T> d2)     const override { return invoke_impl(std::move(d1), std::move(d2)); }
    FunctionResult<T> operator()(Number<T> d1,     VectorView<T> d2) const override { return invoke_impl(std::move(d1), std::move(d2)); }
    FunctionResult<T> operator()(Number<T> d1,     Vector<T>&& d2)   const override { return invoke_impl(std::move(d1), std::move(d2)); }
    FunctionResult<T> operator()(VectorView<T> d1, Number<T> d2)     const override { return invoke_impl(std::move(d1), std::move(d2)); }
    FunctionResult<T> operator()(Vector<T>&& d1,   Number<T> d2)     const override { return invoke_impl(std::move(d1), std::move(d2)); }
    FunctionResult<T> operator()(VectorView<T> d1, VectorView<T> d2) const override { return invoke_impl(std::move(d1), std::move(d2)); }
    FunctionResult<T> operator()(Vector<T>&& d1,   Vector<T>&& d2)   const override { return invoke_impl(std::move(d1), std::move(d2)); }

    FunctionResult<T> invoke_impl(Data<T>&& data1, Data<T>&& data2) const
    {
        m_train.add(std::move(data2));
        m_train.add(std::move(data1));
        auto ret = FunctionResult<T>::fromDataVector(applyFunctionPass(m_train));
        m_train.elements.pop_back();
        m_train.elements.pop_back(); // train restored
        return ret;
    }

private:
    mutable Train<T> m_train;
};

// Functions for evaluation and creating functions
template <typename... Elements>
auto eval(Elements... args)
{
    auto train = Train{std::forward<Elements>(args)...};
    applyCombinatorPass(train);
    return applyFunctionPass(train);
}

template <typename... Elements>
auto defUnaryFun(Elements... args) { return UnaryFunctionFromTrain{Train{std::forward<Elements>(args)...}}; }

template <typename... Elements>
auto defBinaryFun(Elements... args) { return BinaryFunctionFromTrain{Train{std::forward<Elements>(args)...}}; }

} // namespace alco
