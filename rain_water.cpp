// Trapping Rain Water
// ###################
//
// Robert Frysch 2024
//

#include "alco.hpp"

struct Trap
{
    int height{};
    int water{};
};

std::vector<Trap> trapsFromHeights(std::span<const int> heights);
std::vector<Trap> trapsFromHeightsAndWater(std::span<const int> heights, std::span<const int> water);
void printTraps(std::span<const Trap> traps);


int main()
try
{
    const auto input = std::vector<int>{2, 0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1, 4};
    std::cout << "Given:\n";
    printTraps(trapsFromHeights(input));

    // Uiua solution
    // =============
    //
    // MaxScan    ← \↥
    // RevMaxScan ← ⇌\↥⇌
    // FillWater  ← - ⊃∘(↧ ⊃RevMaxScan MaxScan)
    // /+

    using namespace alco;
    using T = int;
    const auto maxScan    = defUnaryFun(Scan<T>{}, Max<T>{});
    const auto revMaxScan = defUnaryFun(Reverse<T>{}, Scan<T>{}, Max<T>{}, Reverse<T>{});
    const auto fillWater  = defUnaryFun(Minus<T>{}, Fork<T>{}, Identity<T>{},
                                                               defUnaryFun(Min<T>{}, Fork<T>{}, revMaxScan, maxScan));

    const auto water = fillWater(input).getVector();

    std::cout << "\nSolution:\n";
    printTraps(trapsFromHeightsAndWater(input, water));
    std::cout << "-> The water capacity is "
              << defUnaryFun(Reduction<T>{}, Plus<T>{})(water).getNumber() << ".\n\n";

} catch(const std::exception& e)
{
    std::cerr << "exception caught: " << e.what();
}


std::vector<Trap> trapsFromHeights(std::span<const int> heights)
{
    auto ret = std::vector<Trap>(size(heights));
    std::ranges::transform(heights, begin(ret), [](auto h) { return Trap{h, 0}; });
    return ret;
}

std::vector<Trap> trapsFromHeightsAndWater(std::span<const int> heights, std::span<const int> water)
{
    auto ret = std::vector<Trap>(size(heights));
    std::ranges::transform(heights, water, begin(ret), [](auto h, auto w) { return Trap{h, w}; });
    return ret;
}

void printTraps(std::span<const Trap> traps)
{
    const auto material = [](const Trap& trap, int altitude) {
        return altitude <= trap.height              ? '#'
             : altitude <= trap.height + trap.water ? ','
                                                    : ' ';
    };

    const auto minHeight = std::ranges::min(traps, std::less{}, std::mem_fn(&Trap::height)).height;
    const auto maxHeight = std::ranges::max(traps, std::less{}, std::mem_fn(&Trap::height)).height;

    for(auto h = maxHeight; h > minHeight; --h)
    {
        for(const auto& trap : traps)
        {
            std::cout << material(trap, h);
        }
        std::cout << '\n';
    }

    // print "ground"
    for(const auto& _ : traps)
    {
        std::cout << '-';
    }
    std::cout << '\n';
}
