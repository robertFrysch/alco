#include "examples.hpp"
#include <iostream>

// Alco - A stack-based algorithm combinator
// =========================================
//
// # Available Functions
// ## Unary
// > Duplicate
// > Pop
// > Identity
// > Length
// > Range
// > Reverse
// > MaxElement, MinElement
// > Negate, Abs
// > Sqrt, Exp, Log
// > Sin, Cos, Tan, ASin, ACos, ATan
// > Floor, Ceil, Round
// ## Binary
// > Flip, Join, Over
// > Drop, Take, Keep
// > Find
// > Equal, NotEqual, Less, LessEqual, Greater, GreaterEqual
// > Plus, Minus, Multiplies, Divides, Modulus
// > Min, Max
// > Pow
//
// # Available Combinators
// ## Unary
// > Reduction
// > Scan
// > Both
// ## Binary
// > Fork
//
// # Functions for usage
// > eval
// > defUnaryFun
// > defBinaryFun
// > print
// > getName, getArity


int main()
try
{
    const auto numFailedExamples = runAllExamples();
    std::cout << "\n#################################\n";

    if(numFailedExamples)
        std::cout << numFailedExamples << " example(s) failed.\n\n";
    else
        std::cout << "All examples passed successfully.\n\n";

    return numFailedExamples;

} catch(const std::exception& e)
{
    std::cerr << "exception caught: " << e.what();
}

