#include "alco.hpp"

using namespace alco;

template <ArithmeticType T>
int checkResult(const FunctionResult<T>& res, std::initializer_list<T> groundTruth)
{
    if(std::ranges::equal(res.getVectorView(), groundTruth))
        return 0;

    print(res);
    std::cout << " != ";
    print(std::span{groundTruth});

    return 1;
}

template <ArithmeticType T>
int checkResult(const FunctionResult<T>& res, T groundTruth)
{
    if(std::abs(res.getNumber() - groundTruth) <= std::numeric_limits<T>::epsilon())
        return 0;

    print(res);
    std::cout << " != " << groundTruth << '\n';

    return 1;
}

int standardDeviation()
{
    std::cout << "\n### Standard Deviation ###\n";
    // https://www.youtube.com/watch?v=Hecqsl3GG9s

    // ÷⊃⧻/+
    const auto average = defUnaryFun(Divides{}, Fork{}, Length{}, Reduction{}, Plus{});

    // √avgⁿ2-avg.
    const auto stdDev = defUnaryFun(Sqrt{}, average, Pow{}, 2., Minus{}, average, Duplicate{});

    print(stdDev.train());

    return checkResult(stdDev({1., 2., 3., 4., 5.}), 1.4142135623730951);
}

int duplicateZeros()
{
    std::cout << "\n### Duplicate Zeros ###\n";
    // https://www.youtube.com/watch?v=iTC1EiX5bM0

    // DupZeros ← ↙⧻:▽+1=0..
    //
    // DupZeros [1 0 2 3 0 4 5 0]
    // DupZeros [1 2 3]
    // DupZeros [0 3 0 4 5]

    //     [1 0 0 2 3 0 0 4]
    //     [1 2 3]
    //     [0 0 3 0 0]

    using T = int;
    const auto fun = defUnaryFun(
        Take<T>{}, Length<T>{}, Flip<T>{},
        Keep<T>{}, Plus<T>{}, 1, Equal<T>{}, 0, Duplicate<T>{}, Duplicate<T>{});

    print(fun.train());

    return checkResult(fun({1, 0, 2, 3, 0, 4, 5, 0}), {1, 0, 0, 2, 3, 0, 0, 4})
         + checkResult(fun({1, 2, 3}), {1, 2, 3})
         + checkResult(fun({0, 3, 0, 4, 5}), {0, 0, 3, 0, 0});
}

int sumOfSquares()
{
    std::cout << "\n### Sum Of Squares ###\n";
    // https://www.youtube.com/watch?v=SpZJxbOf_jM

    // SumOfSquares ← /+ⁿ2×=0◿+1⇡.⧻.
    //
    // SumOfSquares [1 2 3 4]
    // SumOfSquares [2 7 1 19 19 3]

    // 21
    // 63

    using T = int;
    const auto fun = defUnaryFun(
        Reduction<T>{}, Plus<T>{}, Pow<T>{}, 2,
        Multiplies<T>{}, Equal<T>{}, 0, Modulus<T>{}, Plus<T>{}, 1,
        Range<T>{}, Duplicate<T>{}, Length<T>{}, Duplicate<T>{});

    print(fun.train());

    return checkResult(fun({1, 2, 3, 4,}), 21)
         + checkResult(fun({2, 7, 1, 19, 19, 3}), 63);
}

int hypot()
{
    std::cout << "\n### Hypot ###\n";
    // https://www.youtube.com/watch?v=Wah-uQ9ou80

    // Hypot ← √/+ⁿ2⊂
    // Hypot ← √+∩(ⁿ2)
    //
    // Hypot 3 4
    // Hypot 5 12

    // 5
    // 13

    using T = int;
    const auto fun1 = defBinaryFun(
        Sqrt<int>{}, Reduction<T>{}, Plus<T>{}, Multiplies<T>{}, Duplicate<T>{}, Join<T>{});
    const auto fun2 = defBinaryFun(
        Sqrt<int>{}, Plus<T>{}, Both<T>{}, defUnaryFun(Pow<T>{}, 2));

    print(fun1.train());
    print(fun2.train());

    return checkResult(fun1(3, 4), 5)
         + checkResult(fun2(3, 4), 5)
         + checkResult(fun1(5, 12), 13)
         + checkResult(fun2(5, 12), 13);
}


int runAllExamples()
{
    auto numFailedExamples = 0;

    numFailedExamples += duplicateZeros();
    numFailedExamples += sumOfSquares();
    numFailedExamples += hypot();
    numFailedExamples += standardDeviation();

    return numFailedExamples;
}
