# Alco

Alco is a stack-based **al**gorithm **co**mbinator C++20 library (header-only) that can compose data, functions and combinators along the lines of [Uiua](https://www.uiua.org), where
- a `Data` object can be a scalar number or an array,
- a `Function` takes data objects as arguments and returns one or multiple data objects, and
- a `Combinator` (called modifiers in Uiua) is a higher order function that takes functions as arguments and returns a function.

In Alco such a composition is called a *train*. A train can be directly evaluated by using the `eval` function:
```cpp
std::vector<alco::Data<int>> result = alco::eval(alco::Plus<int>{}, 40, 2);
alco::print(result); // prints 42
```
Here, the arguments `(alco::Plus<int>{}, 40, 2)` form a train. Note that a train can only operate on one data type (in this case `int`). When the data type is omitted, it defaults to `double`:
```cpp
alco::eval(alco::Divides{}, 2., 12.);
```
Like in Uiua, the evaluation of a train goes from right to left, i.e. first `12.`, than `2.` and than `Divides` is pushed on the stack. When a function is applied, elements of the stack are popped and passed as function arguments in the order in which they have been pushed to the stack. The result of the function is finally pushed back to the stack. Therefore, the result of the above evaluation is `6`.  
A key feature of Alco is the definition of function objects (based on a given `Train`). For example, you may define the following unary function (function with one parameter):
```cpp
auto tenthOf = alco::defUnaryFun(alco::Divides{}, 10.);
std::cout << tenthOf(42).getNumber(); // 4.2
```
Another key feature is that a data object can also be an one-dimensional array, in particular a `std::vector` or `std::span` can be consumed by Alco. Operations between scalar numbers and arrays can usually be mixed. In the following, we omit the `alco` namespace:
```cpp
auto twiceTheDiff = defBinaryFun(Multiplies{}, 2., Minus{});
print(twiceTheDiff({1., 2., 3.}, 6.)); // prints [ -10 -8 -6 ]
```
With `print(twiceTheDiff.train())`, the wrapped train of the function is printed to `stdout`, which looks as follows:
```bash
Multiplies_a2 Number_a0 Minus_a2
```
where the suffix '_a*n*' specifies the arity (number of arguments), that is consumed by the element.

Finally, functions can be formed by combinators. For instance, the combinator `Reduction` can take a binary function and can reduce an array with it:
```cpp
eval(Reduction{}, Multiplies{}, std::vector{1., 2., 5.}); // evaluates to 10
```

A slightly more elaborated example is this composition of a standard deviation function:
```cpp
auto average = defUnaryFun(Divides{}, Fork{}, Length{}, Reduction{}, Plus{});
auto stdDev = defUnaryFun(Sqrt{}, average, Pow{}, 2., Minus{}, average, Duplicate{});
    
std::array testData{1., 2., 3., 4., 5.};
std::cout << stdDev(testData).getNumber(); // 1.41421
```

See further examples in [examples.cpp](examples.cpp), which are translations of Uiua problem solutions by Conor Hoekstra from his [Youtube channel](https://www.youtube.com/@code_report).
